from tkinter import *

class TelaLogin(Frame):
    def __init__(self,root):
        super().__init__(root)
        frame = Frame(self)
        frame.pack()
        frame.place(anchor='c',relx=0.5,rely=0.5)

        labelNome = Label(frame,text='Login:')
        labelNome.grid(row=0,column=0,pady=3)
        self.entryLogin = Entry(frame)
        self.entryLogin.grid(row=0,column=1,pady=3)

        labelSenha = Label(frame,text='Password:')
        labelSenha.grid(row=1,column=0,pady=3)
        self.entrySenha = Entry(frame,show="*")
        self.entrySenha.grid(row=1,column=1,pady=3)

        botao = Button(frame,text='Logar',width=10,command=self.login)
        botao.grid(row=2,column=0, columnspan=2,pady=6)

    def login(self):
        login = self.entryLogin.get()
        senha = self.entrySenha.get()
        print(login,senha)
        self.destroy()
