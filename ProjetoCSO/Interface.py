from tkinter.ttk import Notebook
from pandastable import Table
from TelaLogin import *
from TelaT3 import *
import pandas as pd

class MeuBotao(Button):
    def __init__(self,root,text="",command=None,state=NORMAL,image=None):
        if image is None:
            super().__init__(root, text=text, command=command, width=15, height=5, state=state)
        else:
            photo = PhotoImage(file=image)
            small_logo = photo.subsample(50, 50)
            super().__init__(root,image=small_logo,text=text,command=command,width=100,height=100,state=state,compound=TOP)
            self.image = small_logo

class TelaInicial(Tk):
    framePrincipal = None
    frameAba = None

    def __init__(self):
        super().__init__()

        #Frame Botoes
        self.framePrincipal = Frame(self,width=200,height=500)
        self.framePrincipal.pack(side=LEFT,expand=False)
        #Frame abas
        self.tabs = Notebook(self, width=500, height=500)
        self.tabs.pack()

        #Iniciando Botões
        #Frame para ficar centralizado
        frame1 = Frame(self.framePrincipal)
        frame1.pack()
        frame1.place(anchor='c',relx=0.5,rely=0.5)
        #Botoes
        self.btInciarCSO = MeuBotao(frame1, text='Iniciar CSO', command=self.iniciarCSO,image="images/login.png")
        self.btInciarCSO.pack(pady=3)
        self.btImportar = MeuBotao(frame1,text='Importar Tabela',command=self.buscarExcel)
        self.btImportar.pack(pady=3)
        self.btT3 = MeuBotao(frame1,text='T3',state = DISABLED,command=self.telaT3)
        self.btT3.pack(pady=3)

        self.iniciarCSO()
        btTeste = MeuBotao(frame1, text='Teste')
        btTeste.pack(pady=3)

    def buscarExcel(self):
        tabelaPath = filedialog.askopenfilename(initialdir="../", title="Select file",
                                  filetypes=(("Excel", "*.xlsx"), ("csv", "*.csv")))
        if 'xlsx' in tabelaPath:
            excel = pd.ExcelFile(tabelaPath)
            print(excel.sheet_names)
            self.lerExcel(excel)

        if 'csv' in tabelaPath:
            self.tabela = pd.read_csv(tabelaPath)
            self.criarTabela(self.tabela,tabelaPath.split('/').pop())

    def lerExcel(self,excel):

        for aba in excel.sheet_names:
            self.tabela = pd.read_excel(excel,aba)
            novoNotebook = Notebook(self.frameAba)
            tabelaComp = Table(novoNotebook, dataframe=self.tabela, showtoolbar=True, showstatusbar=True)
            tabelaComp.show()
            self.tabs.add(novoNotebook, text=aba)
            self.tabs.pack()
        self.btT3['state'] = NORMAL
        tamanho = len(self.tabs.tabs())
        self.tabs.select(tamanho-1)

    def criarTabela(self,df,nome):
        print("cheguei")
        novoNotebook = Frame(self.frameAba)
        self.tabs.add(novoNotebook, text=nome)
        self.tabs.pack()

    def iniciarCSO(self):
        tabs = self.tabs.tabs()
        if "Login" in tabs:
            self.tabs.select(tabs.index("Login"))
        abaLogin = TelaLogin(self.frameAba)
        self.tabs.add(abaLogin, text="Login")
        self.tabs.pack()
        tamanho = len(tabs)
        self.tabs.select(tamanho)

    def telaT3(self):
        tabs = self.tabs.tabs()
        if "T3" in tabs:
            self.tabs.select(tabs.index("T3"))
        abaT3 = TelaT3(self.frameAba,self.tabela)
        self.tabs.add(abaT3, text="T3")
        self.tabs.pack()
        tamanho = len(tabs)
        self.tabs.select(tamanho)











