from tkinter import *
import os
import time
import keyboard

class TelaT3(Frame):
    def __init__(self,root,tabela):
        super().__init__(root)
        self.tabela = tabela

        frame = Frame(self)
        frame.pack()
        frame.place(anchor='c',relx=0.5,rely=0.5)

        labelNumLote = Label(frame,text='Nº Lote:')
        labelNumLote.grid(row=0,column=0,pady=3)
        self.entryNumLote = Entry(frame)
        self.entryNumLote.grid(row=0,column=1,pady=3)

        labelFormaCobranca = Label(frame,text='Foma cobraça:')
        labelFormaCobranca.grid(row=1,column=0,pady=3)
        self.opcao = StringVar(self)
        listaOpcoes = {"Meio Magnético", "Fatura", "Febraban"}
        self.popupMenu = OptionMenu(frame, self.opcao, *listaOpcoes)
        self.popupMenu.grid(row=1, column=1, pady=3)

        labelTelefone = Label(frame, text='Telefone resposavel:')
        labelTelefone.grid(row=2, column=0, pady=3)
        self.entryTelefone = Entry(frame, show="*")
        self.entryTelefone.grid(row=2, column=1, pady=3)

        labelData = Label(frame, text='Data:')
        labelData.grid(row=3, column=0, pady=3)
        self.entryData = Entry(frame, show="*")
        self.entryData.grid(row=3, column=1, pady=3)

        botao = Button(frame,text='Iniciar',width=10,command=self.iniciar)
        botao.grid(row=4,column=0, columnspan=2,pady=6)

    def iniciar(self):
        os.startfile(r'C:\Windows\system32\notepad.exe')
        # keyboard.press_and_release('alt+tab')
        time.sleep(1)
        for linhaNum in range(len(self.tabela)):
            cnl = self.tabela.loc[linhaNum]['CNL']
            # print(self.tabela.loc[linhaNum]['CNL'])
            keyboard.write('5')
            keyboard.press_and_release('tab')
            keyboard.write(str(cnl))
            keyboard.press_and_release('tab')
            keyboard.write("Login")
            keyboard.press_and_release('tab')
            keyboard.write("Senha")
            keyboard.press_and_release('enter')
        keyboard.press_and_release('alt+tab')